package inacap.test.holamundoiei4d.vista.fragmentos;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.StringDef;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.jar.JarException;

import inacap.test.holamundoiei4d.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link BienvenidaFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link BienvenidaFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BienvenidaFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public BienvenidaFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment BienvenidaFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static BienvenidaFragment newInstance(String param1, String param2) {
        BienvenidaFragment fragment = new BienvenidaFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Conexion con la capa visual del fragmento (archivo xml)
        // Aqui podemos llamar a los componentes como Button, TextView, etc.
        View layout = inflater.inflate(R.layout.fragment_bienvinido, container, false);
        Button btTestHTTP = (Button) layout.findViewById(R.id.btTestHTTP);
        btTestHTTP.setOnClickListener(new View.OnClickListener() {
                                          @Override
                                          public void onClick(View view) {
                                              // servidor: www.mindicador.cl
                                              // recurso: /api
                                              // metodo: GET

                                              // Recurso o URL
                                              String url = "http://www.mindicador.cl/api";

                                              // Crear o definir la solicutud
                                              StringRequest solicitud = new StringRequest(
                                                      Request.Method.GET,
                                                      url,
                                                      new Response.Listener<String>() {
                                                          @Override
                                                          public void onResponse(String response) {
                                                              //respuesta exitosa
                                                              Log.e("HTTP", "Respuesta: " + response);
                                                              //Buscar el calor del dolar
                                                              //Leer codigo del JSON
                                                              try {
                                                                  JSONObject jsonReponse = new JSONObject(response);
                                                                  String version = jsonReponse.getString("version");

                                                              } catch (JSONException e) {
                                                                  e.printStackTrace();
                                                              }
                                                          }
                                                      },
                                                      new Response.ErrorListener() {

                                                          @Override
                                                          public void onErrorResponse(VolleyError error) {
                                                              //Error al conectarnos al servidor
                                                              Log.e("HTTP", "El Error al conectar con el servidor");
                                                          }
                                                      }
                                              );

            //Agregar nuetra solucitud a la cola de soluciones
            // Crear la lista de solucitudes
            RequestQueue listaEspera = Volley.newRequestQueue(getActivity());
            listaEspera.add(solicitud);
        }


    });
        return layout;
}

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction("", "");
        }
    }

    @Override

    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(String fragmentName, String action);
    }
}
